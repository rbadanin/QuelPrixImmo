package fr.univpau.quelpriximmo.model;

import java.io.Serializable;
import java.util.Date;

public class BienImmo implements Serializable {
    private Integer prix;
    private String formattedAddress;
    private Double Lat;
    private Double Lng;
    private String type;
    private Integer nPiece;
    private Integer surface;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private Date date;

    public Integer getDistanceToMe() {
        return distanceToMe;
    }

    public void setDistanceToMe(Integer distanceToMe) {
        this.distanceToMe = distanceToMe;
    }

    private Integer distanceToMe;

    public Integer getPrix() {
        return prix;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public Double getLat() {
        return Lat;
    }

    public void setLat(Double lat) {
        Lat = lat;
    }

    public Double getLng() {
        return Lng;
    }

    public void setLng(Double lng) {
        Lng = lng;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getnPiece() {
        return nPiece;
    }

    public void setnPiece(Integer nPiece) {
        this.nPiece = nPiece;
    }

    public Integer getSurface() {
        return surface;
    }

    public void setSurface(Integer surface) {
        this.surface = surface;
    }



}
