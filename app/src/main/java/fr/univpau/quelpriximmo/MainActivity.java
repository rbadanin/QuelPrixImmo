package fr.univpau.quelpriximmo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.univpau.quelpriximmo.model.BienImmo;

public class MainActivity extends AppCompatActivity {

    private Menu menu;

    public Fragment getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment(Fragment currentFragment) {
        this.currentFragment = currentFragment;
    }

    private Fragment currentFragment;

    private List<BienImmo> listBiens;
    private Double currentLat;
    private Double currentLong;

    public void setCurrentData(List<BienImmo> listBiens, Double currentLat, Double currentLong)
    {
        this.listBiens = listBiens;
        this.currentLat = currentLat;
        this.currentLong = currentLong;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);

        final Context ct = this;

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent();

                Integer count1 = 0;
                Integer count2 = 0;
                Integer count3 = 0;
                Integer count4 = 0;
                Integer count5 = 0;
                Integer count6 = 0;
                Integer count7 = 0;
                Integer count8 = 0;



                for(BienImmo bn : listBiens)
                {
                    Integer prix = bn.getPrix();

                    if(prix > 0 && prix <= 50000)
                    {
                        count1++;
                    }
                    else if(prix > 50000 && prix <= 100000)
                    {
                        count2++;
                    }
                    else if(prix > 100000 && prix <= 150000)
                    {
                        count3++;
                    }
                    else if(prix > 150000 && prix <= 200000)
                    {
                        count4++;
                    }
                    else if(prix > 200000 && prix <=  300000)
                    {
                        count5++;
                    }
                    else if(prix > 300000 && prix <=  400000)
                    {
                        count6++;
                    }
                    else if(prix > 400000 && prix <= 500000)
                    {
                        count7++;
                    }
                    else if(prix > 500000)
                    {
                        count8++;
                    }

                }


                it.putExtra("0a50", count1);
                it.putExtra("50a100", count2);
                it.putExtra("100a150", count3);
                it.putExtra("150a200", count4);
                it.putExtra("200a300", count5);
                it.putExtra("300a400", count6);
                it.putExtra("40a500", count7);
                it.putExtra("plus500", count8);

                it.setClass(ct, GraphActivity.class);
                startActivity(it);
            }
        });
    }

    public void toggleGraphActionButton(int state){
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setVisibility(state);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    public void toggleResultMenu(boolean bool)
    {
        menu.setGroupVisible(R.id.sortMenu, bool);
        menu.setGroupVisible(R.id.mapMenu, bool);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        if (id == android.R.id.home) {
            //Title bar back press triggers onBackPressed()
            onBackPressed();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false); // On cache le bouton back quand on est sur la 1ere page
            return true;
        }

        if (id == R.id.action_map) {

            Intent it = new Intent();
            it.setClass(this, MapsActivity.class);

            it.putExtra("lat", currentLat);
            it.putExtra("lon", currentLong);


            it.putExtra("listBiens", (Serializable) (new ArrayList<BienImmo>(listBiens.subList(0, listBiens.size() >= 100 ? 100 : listBiens.size()))));

            startActivity(it);
            return true;
        }


        if(id == R.id.sortDistance && currentFragment instanceof ResultListFragment)
        {
            ((ResultListFragment) currentFragment).doSort("sortDistance");
        }

        if(id == R.id.sortDate && currentFragment instanceof ResultListFragment)
        {
            ((ResultListFragment) currentFragment).doSort("sortDate");
        }


        if(id == R.id.sortPrice && currentFragment instanceof ResultListFragment)
        {
            ((ResultListFragment) currentFragment).doSort("sortPrice");
        }


        if(id == R.id.sortRooms && currentFragment instanceof ResultListFragment)
        {
            ((ResultListFragment) currentFragment).doSort("sortRooms");
        }


        if(id == R.id.sortSquareMeterPrice && currentFragment instanceof ResultListFragment)
        {
            ((ResultListFragment) currentFragment).doSort("sortSquareMeterPrice");
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0 ) {
            getFragmentManager().popBackStack();
        }
        else {
            super.onBackPressed();
        }
    }

}