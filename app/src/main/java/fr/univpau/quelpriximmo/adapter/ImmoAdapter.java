package fr.univpau.quelpriximmo.adapter;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import fr.univpau.quelpriximmo.R;
import fr.univpau.quelpriximmo.model.BienImmo;

public class ImmoAdapter extends ArrayAdapter<BienImmo> {
    private Context context;

    public ImmoAdapter(Context context, List<BienImmo> biens) {
        super(context, 0, biens);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_immo, parent, false);
        }

        ImmoHolder viewHolder = (ImmoHolder) convertView.getTag();

        if(viewHolder == null){
            viewHolder = new ImmoHolder();
            viewHolder.contentText = (TextView) convertView.findViewById(R.id.contentText);
            viewHolder.headerText = (TextView) convertView.findViewById(R.id.headerText);
            viewHolder.footerText = (TextView) convertView.findViewById(R.id.footerText);
            viewHolder.mapButton = (Button) convertView.findViewById(R.id.mapButton);

            convertView.setTag(viewHolder);
        }

        BienImmo immo = getItem(position);


        viewHolder.headerText.setText(String.format("%s - à %sm de moi", immo.getFormattedAddress(), NumberFormat.getNumberInstance(Locale.FRANCE).format(immo.getDistanceToMe())));
        viewHolder.contentText.setText(String.format("%s - %d pièces - %s m²", immo.getType(), immo.getnPiece(), NumberFormat.getNumberInstance(Locale.FRANCE).format(immo.getSurface())));

        SimpleDateFormat parser= new SimpleDateFormat("dd/MM/yyyy");
       if(immo.getPrix() == -1)
       {

           if(immo.getDate() == null)
           {
               viewHolder.footerText.setText("Prix Inconnu");
           }
           else
           {
               viewHolder.footerText.setText(String.format("Vendu le %s à un prix inconnu", parser.format(immo.getDate())));
           }
       }
       else
       {
           Integer valueParMetre = Math.round(immo.getPrix() / immo.getSurface());

           if(immo.getDate() == null)
           {
               viewHolder.footerText.setText(String.format("Vendu %s € - %s €/m²", NumberFormat.getNumberInstance(Locale.FRANCE).format(immo.getPrix()), NumberFormat.getNumberInstance(Locale.FRANCE).format(valueParMetre)));
           }
           else
           {

               viewHolder.footerText.setText(String.format("Vendu %s € le %s - %s €/m²", NumberFormat.getNumberInstance(Locale.FRANCE).format(immo.getPrix()), parser.format(immo.getDate()) ,NumberFormat.getNumberInstance(Locale.FRANCE).format(valueParMetre)));
           }

       }

        viewHolder.mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strUri = "http://maps.google.com/maps?q=loc:" + immo.getLat() + "," + immo.getLng() + " (" + immo.getFormattedAddress() + ")";
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                context.startActivity(intent);
            }
        });

        return convertView;
    }
}
