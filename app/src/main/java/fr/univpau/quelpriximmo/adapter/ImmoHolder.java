package fr.univpau.quelpriximmo.adapter;

import android.widget.Button;
import android.widget.TextView;

public class ImmoHolder {
    public TextView headerText;
    public TextView contentText;
    public TextView footerText;
    public Button mapButton;
}
