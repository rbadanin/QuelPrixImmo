package fr.univpau.quelpriximmo;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import fr.univpau.quelpriximmo.model.BienImmo;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        Snackbar snack = Snackbar.make(this.getWindow().getDecorView().findViewById(android.R.id.content)
                , "Affichage des 100 biens les plus proche", Snackbar.LENGTH_LONG);


        snack.show();

        mapFragment.getMapAsync(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Carte des biens");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            //Title bar back press triggers onBackPressed()
            onBackPressed();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0 ) {
            getFragmentManager().popBackStack();
        }
        else {
            super.onBackPressed();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Context mContext = this;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                LinearLayout info = new LinearLayout(mContext);
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(mContext);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(mContext);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        });

        Double lat = getIntent().getDoubleExtra("lat", 0);
        Double lon = getIntent().getDoubleExtra("lon",0);

        List<BienImmo> listBiens = (List<BienImmo>)getIntent().getSerializableExtra("listBiens");

        LatLng me = new LatLng(lat, lon);

        for(BienImmo bien : listBiens)
        {
            StringBuilder title = new StringBuilder();

            String header = String.format("%s - à %sm de moi", bien.getFormattedAddress(), NumberFormat.getNumberInstance(Locale.FRANCE).format(bien.getDistanceToMe()));

            title.append(String.format("%s - %d pièces - %s m²", bien.getType(), bien.getnPiece(), NumberFormat.getNumberInstance(Locale.FRANCE).format(bien.getSurface())));
            title.append("\n");

            SimpleDateFormat parser= new SimpleDateFormat("dd/MM/yyyy");
            if(bien.getPrix() == -1)
            {

                if(bien.getDate() == null)
                {
                    title.append("Prix Inconnu");
                    title.append("\n");
                }
                else
                {
                    title.append(String.format("Vendu le %s à un prix inconnu", parser.format(bien.getDate())));
                    title.append("\n");
                }
            }
            else
            {
                Integer valueParMetre = Math.round(bien.getPrix() / bien.getSurface());

                if(bien.getDate() == null)
                {
                    title.append(String.format("Vendu %s € - %s €/m²", NumberFormat.getNumberInstance(Locale.FRANCE).format(bien.getPrix()), NumberFormat.getNumberInstance(Locale.FRANCE).format(valueParMetre)));
                }
                else
                {

                    title.append(String.format("Vendu %s € le %s - %s €/m²", NumberFormat.getNumberInstance(Locale.FRANCE).format(bien.getPrix()), parser.format(bien.getDate()) ,NumberFormat.getNumberInstance(Locale.FRANCE).format(valueParMetre)));
                }

            }

            LatLng bienPin = new LatLng(bien.getLat(), bien.getLng());
            mMap.addMarker(new MarkerOptions().position(bienPin).title(header).snippet(title.toString()));
        }

        mMap.addMarker(new MarkerOptions().position(me).title("Moi").flat(false));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(me, 15.0f));


    }
}